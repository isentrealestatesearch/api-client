## What does it do

Browse google map to find real estate properties.

## How to run

 - clone repo
 - run `npm install`
 - find `.env.sample` file in a root folder and rename it to `.env`
 - edit `.env` file: 
  1. REACT_APP_BASE_URL={your backend api url}. Default is `https://localhost:7108`
  2. REACT_APP_MAP_KEY={your google maps api key}

 - backend server app must be running

## Commands

 - `start`: `react-scripts start`
 - `build`: `react-scripts build`
 - `test`: `react-scripts test`
 - `eject`: `react-scripts eject`

## Assumptions
 1. By default map is centere in Zurich showing all real estate properties
 2. Users might want more useful info than just a title on marker. Hence, more available info was added to marker on click.

## Future improvements

 1. More unit tests.
 2. Integration tests.
 3. Research lowering amount of server requests. There's already a debounce but also other improvements may be available.
 4. Mobile UI support.
 5. Optimize react rendering by using marker id as a react key. Backend data model should introduce id for real estate entity.
