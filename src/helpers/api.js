async function queryProperties(query) {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_BASE_URL}/RealEstate`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(query),
      }
    );
    return response.json();
  } catch (error) {
    console.error(error);
  }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default { queryProperties };
