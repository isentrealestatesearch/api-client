export function mapPropertyToMarker({
  title,
  streetName,
  streetNumber,
  zip,
  city,
  location: { latitude, longitude } = {},
} = {}) {
  return {
    title,
    description: `${streetName} ${streetNumber}<br/>${zip} ${city}`,
    position: {
      lat: Number(latitude),
      lng: Number(longitude),
    },
  };
}
