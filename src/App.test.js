import { render, screen } from "@testing-library/react";
import App from "./App";

test('renders "Property type" select with default option "all" selected', () => {
  render(<App />);
  const element = screen.getByLabelText("Property type");
  expect(element).toBeVisible();
  expect(element).toHaveTextContent("all");
});
