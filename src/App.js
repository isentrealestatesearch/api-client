import React, { useEffect } from "react";
import Grid from "@mui/material/Grid";

import { mapPropertyToMarker } from "./helpers/mapPropertyToMarker";
import Map from "./components/map/Map";
import Select from "./components/select/Select";
import api from "./helpers/api";

const PROPERTY_TYPES = [
  {
    key: 0,
    value: "all",
  },
  {
    key: 1,
    value: "Apartment",
  },
  {
    key: 2,
    value: "House",
  },
];

const INITIAL_CENTER_ZURICH = {
  lat: 47.3769,
  lng: 8.5417,
};

const mapContainerStyle = {
   width: "100%",
   height: "100vh"
};

function App() {
  const [markers, setMarkers] = React.useState([]);
  const [query, setQuery] = React.useState({ type: PROPERTY_TYPES[0].key });

  useEffect(() => {
    if (Number.isInteger(query.type) && query.viewPort)
      api
        .queryProperties(query)
        .then((propertiesResponse = []) =>
          setMarkers(propertiesResponse.map(mapPropertyToMarker))
        );
  }, [query]);

  return (
    <div className="App">
      <Grid container spacing={2}>
        <Grid item xs={10}>
          <Map
            markers={markers}
            initialCenter={INITIAL_CENTER_ZURICH}
            mapContainerStyle={mapContainerStyle}
            onChange={(viewPort) => {
              setQuery({ ...query, viewPort });
            }}
          />
        </Grid>

        <Grid item xs={2}>
          <Select
            label="Property type"
            options={PROPERTY_TYPES}
            selectedOptionKey={query.type}
            onChange={({ target: { value: type } = {} }) => {
              setQuery({ ...query, type });
            }}
            data-testid="select-element"
          />
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
