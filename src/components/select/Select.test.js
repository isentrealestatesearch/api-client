// import React from "react";
import { render, screen, fireEvent, within } from "@testing-library/react";
import Select from "./Select";
import userEvent from "@testing-library/user-event";

beforeAll(() => {
  userEvent.setup();
});

test("triggers onChange callback with selection option as first argument", async () => {
  const onChangeMock = jest.fn();
  render(
    <Select
      label={"selectLabel"}
      options={[
        { key: "key1", value: "value1" },
        { key: "key2", value: "value2" },
        { key: "key3", value: "value3" },
      ]}
      selectedOptionKey={"key2"}
      onChange={onChangeMock}
    />
  );

  fireEvent.mouseDown(screen.getByRole("button"));

  const listbox = screen.getByRole("listbox");
  await fireEvent.click(within(listbox).getByText("value3"));

  expect(onChangeMock.mock.calls.length).toBe(1);
});
