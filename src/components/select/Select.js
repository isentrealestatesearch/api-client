import * as React from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import MaterialSelect from "@mui/material/Select";

import "./Select.css";

export default function Select({
  options,
  label,
  selectedOptionKey,
  onChange,
}) {
  return (
    <Box sx={{ minWidth: 120 }} className="input-box">
      <FormControl fullWidth>
        <InputLabel id="label">{label}</InputLabel>
        <MaterialSelect
          labelId="label"
          value={selectedOptionKey}
          label="Property type"
          onChange={onChange}
        >
          {options.map((option) => (
            <MenuItem key={option.key} value={option.key}>
              {option.value}
            </MenuItem>
          ))}
        </MaterialSelect>
      </FormControl>
    </Box>
  );
}
