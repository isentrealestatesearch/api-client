import React from "react";

function Marker(options) {
  const [marker, setMarker] = React.useState();

  React.useEffect(() => {
    if (!marker) {
      setMarker(new window.google.maps.Marker());
    }

    // Remove marker from google map on unmount
    return () => {
      if (marker) {
        marker.setMap(null);
      }
    };
  }, [marker]);

  React.useEffect(() => {
    if (marker) {
      marker.setOptions({
        ...options
      });
    }
  }, [marker, options]);

  const infowindow = React.useMemo(() => {
    const contentString = 
    `<div>
      <h2>${options.title}</h2>
      <p>${options.description}</p>
    </div>`;

    return new window.google.maps.InfoWindow({
      content: contentString,
    });
  }, [options.title, options.description]);

  React.useEffect(() => {
    if (marker) {
      marker.addListener("click", () => {
        infowindow.open({
          anchor: marker,
          map: options.map,
          shouldFocus: false,
        });
      });

      // Remove event handlers on unmount
      return () => {
        window.google.maps.event.clearInstanceListeners(marker, "click");
      }
    }
  }, [infowindow, marker, options.map]);

  return null;
}

export default Marker;
