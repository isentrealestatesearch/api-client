import React from "react";
import { Wrapper } from "@googlemaps/react-wrapper";
import debounce from "lodash.debounce";

import Marker from "../marker/Marker";
import useDeepCompareEffectForMaps from "../../hooks/useDeepCompareEffectForMaps";

function Map({ markers, onChange, initialCenter, mapContainerStyle, ...options }) {
  const [map, setMap] = React.useState();

  const mapContainerRef = React.useCallback(
    (node) => {
      if (node !== null) {
        setMap(
          new window.google.maps.Map(node, {
            zoom: 10,
            center: initialCenter,
          })
        );
      }
    },
    [initialCenter]
  );

  // because React does not do deep comparisons, a custom hook is used
  // see discussion in https://github.com/googlemaps/js-samples/issues/946
  useDeepCompareEffectForMaps(() => {
    if (map) {
      map.setOptions(options);
    }
  }, [map, options]);

  const getViewport = React.useCallback(
    function getViewport() {
      const bounds = map.getBounds();
      return {
        low: {
          latitude: bounds.getSouthWest().lat(),
          longitude: bounds.getSouthWest().lng(),
        },
        high: {
          latitude: bounds.getNorthEast().lat(),
          longitude: bounds.getNorthEast().lng(),
        },
      };
    },
    [map]
  );

  const debouncedOnChange = React.useMemo(
    () => debounce(() => onChange(getViewport()), 500),
    [getViewport, onChange]
  );

  React.useEffect(() => {
    if (map) {
      window.google.maps.event.clearListeners(map, "idle");
      map.addListener("idle", debouncedOnChange);
    }
  }, [map, debouncedOnChange]);

  return (
    <Wrapper apiKey={process.env.REACT_APP_MAP_KEY}>
      <div ref={mapContainerRef} style={mapContainerStyle}>
        {markers.map(({ position, title, description }, i) => (
          <Marker
            key={position.lat + position.lng}
            title={title}
            description={description}
            position={position}
            map={map}
          ></Marker>
        ))}
      </div>
    </Wrapper>
  );
}

export default Map;
